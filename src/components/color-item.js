import React from "react";
import Snackbar from '@material-ui/core/Snackbar';
import './color-item.scss'



function isBright(hex){
  var result = /^#?([A-F\d]{2})([A-F\d]{2})([A-F\d]{2})$/i.exec(hex);

  var r = parseInt(result[1], 16);
  var g = parseInt(result[2], 16);
  var b = parseInt(result[3], 16);

  r = r/255;
  g = g/255;
  b = b/255;
  var max = Math.max(r, g, b), min = Math.min(r, g, b);
  // var h, s, l = (max + min) / 2;
  var l = (max + min) / 2;

  // if(max == min){
  //     h = s = 0; // achromatic
  // } else {
  //     var d = max - min;
  //     s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
  //     switch(max) {
  //         case r: h = (g - b) / d + (g < b ? 6 : 0); break;
  //         case g: h = (b - r) / d + 2; break;
  //         case b: h = (r - g) / d + 4; break;
  //     }
  //     h /= 6;
  // }

  // s = s*100;
  // s = Math.round(s);
  l = l*100;
  l = Math.round(l);

  // var colorInHSL = 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
  // return colorInHSL
  return l > 50
}

export default function ColorItem(props){
    const {color , clicking, carryWidth} = props;
    const [state, setState] = React.useState({
        open: false,
        openWide:false,
        windowState:"normal"
    });
    const colorIsBright = isBright(color)
    const vertical = "top"
    const horizontal = "right"
    const { open, openWide, windowState } = state;
    const spanClick = (e) => {
      e.stopPropagation();
      var textField = document.createElement('textarea')
      textField.innerText = color
      document.body.appendChild(textField)
      textField.select()
      document.execCommand('copy')
      textField.remove()
      setState({ ...state,open: true });
    };

    const itemClick = (e) => {
      clicking()
      if(!carryWidth){
        setState({ ...state,openWide: true });
      }else{
        setState({ ...state,openWide: false });
      }
    }
  
    const handleClose = () => {
      setState({ ...state, open: false });
    };

    React.useEffect(() => {
      // normal, open, closed
      if(!carryWidth){
        setState({...state,windowState:"normal"})
      }else if(carryWidth && openWide){
        setState({...state,windowState:"open"})
      }else{
        setState({...state,windowState:"closed"})
      }
    },[carryWidth,openWide]);
    return (
        <>
            <div className={'item '+(windowState === "open" ? "open" : windowState === "closed" ? "closed" : "" )}
              style={{backgroundColor:color}}
              onClick={itemClick}
              >
              {windowState === "normal" ?
                <span className={'text '+(colorIsBright ? 'dim_text' : 'light_text')} onClick={spanClick}>{color}  </span>
                : windowState === "open" ?
                <span className={'text '+(colorIsBright ? 'dim_text' : 'light_text')} onClick={spanClick}>{color} </span>
                : ''}
            </div>
            
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                key={`${vertical},${horizontal}`}
                open={open}
                autoHideDuration={2000}
                onClose={handleClose}
                message={`copied ${color}`}
            />
        </>
      );
}