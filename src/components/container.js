import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ColorItem from './color-item'
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Button from '@material-ui/core/Button';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import LanguageIcon from '@material-ui/icons/Language';


import FavoriteIcon from '@material-ui/icons/Favorite';



import './container.scss';

const useStyle = makeStyles({
    containerStyle: { backgroundColor:"white"},
    heading : {
        height:"5rem",
        display:"flex",
        alignItems:"center",
        justifyContent:"space-evenly",
        fontSize:"1.8rem",
        boxShadow:"inset 0px 0px 0px 2px #000000",
        fontWeight:"300"
    }
  });

function generateHex(){
    // return '#'+Math.floor(Math.random()*16777215).toString(16).toUpperCase();
    var r = Math.floor(Math.random()*255).toString(16).padStart(2,'0').toUpperCase()
    var g = Math.floor(Math.random()*255).toString(16).padStart(2,'0').toUpperCase()
    var b = Math.floor(Math.random()*255).toString(16).padStart(2,'0').toUpperCase()
    return '#'+r+g+b
}

function generateColor(){
    return generateHex();
}

export default function Container(props){
    // #A4EC12
    const classes = useStyle();
    const matches = useMediaQuery('(min-width:600px)');
    const [state, setState] = React.useState({
        arr: [{"color":generateColor()},{"color":generateColor()},{"color":generateColor()},{"color":generateColor()},{"color":generateColor()}],
        openingMode:false
      });
    const changeColors = () => {
        setState({ ...state,arr: [{"color":generateColor()},{"color":generateColor()},{"color":generateColor()},{"color":generateColor()},{"color":generateColor()}] });
    }
    const handleColorClicks = () => {
        setState({...state, openingMode : !state.openingMode})
    }
    return (
        <>
            <div className={classes.containerStyle}>
                <div className={classes.heading} >
                    Random color generator <Button variant="contained" color="primary" onClick={changeColors}>Generate</Button> 
                </div>
                <div className={'color_item_container '+(matches ? 'horizontal' : 'vertical')} >
                    {!state.openingMode ? 
                    <span className={'webLink clickable'} > <div>Made with&nbsp;&nbsp;<FavoriteIcon style={{color:"#E5E80E"}} />&nbsp;&nbsp;by vickypedia&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    <div><a rel="noopener noreferrer" href="https://iamvickypedia.web.app" target="_blank"><LanguageIcon style={{color:'#56CCBA'}} className={'clickable'}/></a>&nbsp;&nbsp; <a rel="noopener noreferrer" href="https://www.linkedin.com/in/developervikas/" target="_blank"> <LinkedInIcon style={{color:'#5EAFDE'}} className={'clickable'} /></a></div>
                    </span> : null
                    }
                
                    { state.arr.map( (i,index)=>{
                        return <ColorItem key={index} color={i.color} clicking={handleColorClicks} carryWidth={state.openingMode} />
                    }) }
                </div>
            </div>
        </>
      );
}